---
layout: handbook-page-toc
title: "SG.5.06 - Board of Director Bylaws"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SG.5.06 - Board of Director Bylaws

## Control Statement

Bylaws that govern the Board of Directors demonstrate independence from management and oversight into company operations.

## Context

The Board of Directors have a set of bylaws that establishes their governance responsibilities are board memnbers over GitLab. The establishment of these bylaws are published on the GitLab handbook to evidence that the board is independent from management in it's responsibilities for providing oversight into company operations.

## Scope

This is a policy related control and the overall scope is the established bylaws for the Board of Directors.

## Ownership

* Control Owner: `Security Compliance`
* Process owner(s): `Legal`

## Guidance

The bylaws governing the Board of Directors is available publicly via the [Governance Documents](/handbook/board-meetings/bylaws.html) handbook page. While Security Compliance owns the control to ensure that the bylaws exist, the ultimate owner for any updates to the bylaws which govern the board is GitLab Legal.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Board of Director Bylaws control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/-/issues/1710).

### Policy Reference
*  [Governance Documents](/handbook/board-meetings/bylaws.html)

## Framework Mapping

* SOC2 CC
  * CC1.2
